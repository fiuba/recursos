
## R Source Code Blocks in Org Mode
http://orgmode.org/worg/org-contrib/babel/languages/ob-doc-R.html

R is a free software environment for statistical computing and graphics. R source code blocks are fully supported in Org Mode with a wide variety of R-specific header arguments.

R source code blocks in Org Mode can be used to create R packages, carry out statistical analyses, create graphic displays, and produce reproducible research papers.
